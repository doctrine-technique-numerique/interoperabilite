# 1. Préambule

## 1.1. Contexte

Le présent document constitue le référentiel d’interopérabilité des services numériques pour l’éducation. Il s’inscrit dans le corpus des référentiels de la doctrine technique du numérique pour l’éducation.

Il recense les règles, exigences et recommandations d’interopérabilité pour l’ensemble des acteurs qui opèrent les services numériques pour l’éducation. 
Ce document s’appuie sur des documents de référence français et européens : le [référentiel général d’interopérabilité](https://www.numerique.gouv.fr/uploads/Referentiel_General_Interoperabilite_V2.pdf) et l’[EIF (European Interoperability Framework)](https://joinup.ec.europa.eu/interoperable-europe). Il s’inspire des meilleures pratiques en matière de standardisation, d’architecture technique et d’urbanisation de système d’information. Il ne souscrit à aucune méthode ni aucun outil propriétaire.

Ce document s’adresse spécifiquement aux acteurs qui réalisent, exploitent, intègrent les services numériques pour l’éducation pour le compte de porteurs de projet et responsables de traitement (collectivités territoriales, académies, ministère en charge de l’éducation, EPLE). Il vise à ce que chaque acteur puisse apporter des services à valeur ajoutée avec les mêmes règles, et à ce que chaque utilisateur bénéficie de ces services dans le cadre de confiance fixé par le ministère dans la doctrine technique du numérique pour l’éducation.

## 1.2. Objectifs du document

L’objectif du document est de définir des **exigences** en matière d’interopérabilité pour les services numériques pour l’éducation.

Il doit ainsi permettre de **faciliter les choix pour les concepteurs et acheteurs** de services numériques pour l’éducation, et de **renforcer la cohérence de l’écosystème du numérique pour l’éducation aux échelles nationale et européenne**. 

Le document définit donc les **standards et protocoles** du périmètre défini dans la doctrine technique. Les différentes versions du document pourront également présenter des trajectoires et cibles à atteindre à un horizon temporel déterminé.

## 1.3. Cycle de vie du document et gouvernance

Le ministère, ses services déconcentrés et opérateurs, les collectivités territoriales et les fournisseurs de services numériques pour l’éducation ont participé à l’élaboration de ce référentiel. Les enseignants et personnels de direction des établissements scolaires ont également été parties prenantes des travaux pour identifier les besoins des usagers en matière d’amélioration de leurs parcours usagers dans l’écosystème des services et ressources numériques pour l’éducation à leur disposition.

Le présent document constitue la **première version** du référentiel d’interopérabilité des services numériques pour l’éducation.

Ce document a vocation à évoluer fréquemment pour s’adapter aux évolutions technologiques et juridiques à venir. Une mise à jour annuelle du document est prévue à l’instar de la doctrine technique du numérique pour l’éducation. 
