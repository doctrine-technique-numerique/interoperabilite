# 7. Synthèse des standards retenus

![Figure 7 : Synthèse des standards retenus](Figures/Figure7.0.png "Figure 7 : Synthèse des standards retenus")  
*Figure 7 : Synthèse des standards retenus*

| Champs | Organisme | Standard | Observations | Statut |
|---|---|---|---|---|
| Protocole	| IETF	| HTTPS |	HyperText Transfer Protocol Secure : combinaison du HTTP avec une couche de chiffrement TLS.	| Cible |
| Protocole	| |	REST | Representational state transfer. Style d' architecture logicielle définissant un ensemble de contraintes à utiliser pour créer des services web | Cible |
| Format |	IETF |	JSON |	JavaScript Object Notation. Format de données textuel dérivé de la notation des objets du langage JavaScript.	| Cible |
| Format |	W3C |	XML |	Extensible Markup Language. L'objectif initial de XML est de faciliter l'échange automatisé de contenus complexes entre systèmes d'informations hétérogènes	| En usage |
| Protocole |	OIDF |	Open ID Connect (OIDC) |	Couche d'identification basée sur OAuth 2.0, dispositif d'autorisation.	| Cible |
| Protocole	| Internet engineering task force (IETF) |	OAuth2 |	Protocole d’autorisation. Inclus dans OIDC |	Cible |
| Protocole	| Organization for the Advancement of Structured Information Standards (OASIS)	| SAML 2.0 |	Utilisé par le GAR. Permet l’échange de données d’authentification et d’identification entre domaines de sécurité.	| En usage |
| Protocole	| APEREO	| CAS	| Central Authentification Service (système d’authentification unique). Support OIDC, OAuth2 et SAML	| Fin de vie |
| Protocole / format |	1EdTech 	| OneRoster	| Profil français à venir, dans le cadre des travaux avec 1Edtech. Éléments d’échange de toutes les données de vie scolaire	| Cible |
| Format	| IETF	| EML	| Format de fichier pour les messages électroniques	| Cible |
| Format	| Internet Mail Consortium	| vCard	| Format de carnet d’adresses	| Cible |
| Format |	IETF	| iCal	| Internet Calendaring and Scheduling Core Object Specification pour l’échange et le déploiement des événements de calendrier et de planification.	| Cible |
| Données évènementielles / Protocole |	ADIL/IEEE	| xAPI	| Tin Can/xAPI est un standard paramétrable qui peut être utilisé dans tous les contextes (services numériques, plateformes pédagogiques, ressources numériques, évaluations formatives et sommatives, interactions pair à pair) pour collecter des données évènementielles permettant de rendre compte du parcours d’un élève en le suivant pas à pas, et des usages d’une plateforme, d’un service ou d’une ressource. |	Cible |
| Données évènementielles / Protocole |	1EdTech	| Caliper Analytics	| Le standard Caliper offre un cadre normalisé et largement adopté pour la collecte et l'analyse des données événementielles liées à l'apprentissage, favorisant ainsi une meilleure compréhension de l'activité des élèves tout au long de leur parcours éducatif. |	Cible |
| Métadonnées de définitions des contenus / Modèle logique de données et sérialisation	| 1EdTech	| Common Cartridge 	| Une norme de packaging des contenus pour échanger des contenus et des parcours pédagogiques.	| Cible |
| Données pour le conditionnement et l’intégration des contenus / Modèle logique de données et sérialisation	| ADL/IEEE	| Scorm	| Format pour développer et publier des ressources d'apprentissage numériques réutilisables dans des systèmes de gestion de l'apprentissage et d'autres environnements d'apprentissage.	| En fin de vie |
| Données pour le conditionnement et l’intégration des contenus / Modèle logique de données et sérialisation	| Groupe H5P	| H5P	| H5P est un plugin pour les systèmes de publication existants qui permet aux utilisateurs de créer du contenu interactif comme des vidéos interactives, des présentations, des jeux, des quiz et bien d’autres.	| Cible |
| Données pour le conditionnement et l’intégration des contenus	| 1EdTech	| QTI	| Format d’échange de questions et de structures de test, d'informations sur la notation	| Cible |
| Données pour le conditionnement et l’intégration des contenus / Protocole	| 1EdTech	| LTI	| Protocole permettant à un système de gestion de l'apprentissage d'intégrer des outils d'apprentissage personnalisés ou des expériences proposées par un autre système.	| Cible |
| Métadonnées de définitions des contenus / Dictionnaires de données, modèle logique de données et sérialisation	| IEEE / AFNOR	| ScoLOMFR	| Description des ressources. | Cible |
| Modèle de données	| Commission Européenne	| European Learning Model (ELM)	| Modèle de données multilingue pour l'interopérabilité des possibilités d'apprentissage, des qualifications, de l'accréditation et des titres de compétences en Europe. | Cible |
| Modèle de données | 1EdTech | Open Badge | Méthode pour regrouper des informations sur une reconnaissance ou une réalisation unique, telle qu'une microcertification, une aptitude, une compétence ou un diplôme.	| Cible |