# 2. Principes d’interopérabilité 

L’interopérabilité, au-delà du seul périmètre technique, impacte les systèmes informatiques, les processus, les procédures, les organisations tout au long de leur cycle de vie. L'interopérabilité est assurée au travers de normes et formats respectés par tout élément ou système qui souhaite intégrer un domaine interopérable. Si l’interopérabilité dans les domaines de l’informatique et des télécommunications peut également être un outil de positionnement commercial, permettant à des consortiums de collaborer pour offrir des services plus complets qui s’enrichissent les uns avec les autres, c’est aussi un outil au service de l’humain pour lui permettre d’accéder à des parcours usagers plus fluides et proches des besoins. 

## 2.1. Définition

L’interopérabilité s'entend comme la capacité de deux ou plusieurs systèmes à échanger des données et à utiliser les informations de chacun, sans nécessiter d’efforts importants de la part des utilisateurs ou des développeurs pour les intégrer et en faire usage.

Pour ce faire, ces systèmes doivent préalablement s’accorder sur la manière dont ils vont communiquer et utiliser les données et les informations qui peuvent être échangées. Cela consiste à s’entendre sur les protocoles d’échanges à utiliser et sur le format, la signification, la pertinence ou valeur ajoutée des données échangées.

La gestion des identités et des accès selon des règles partagées permet de fournir des parcours usagers sans réauthentification et respectueux des données personnelles, résolvant en cela un premier besoin d’interopérabilité entre les systèmes. Des notions et processus métiers partagés permettent quant à eux de poser les bases d’exigences d’interopérabilité pour les acteurs intervenant dans le même périmètre métier. L'adoption de normes ou de standards identiques au sein d'un écosystème favorise une autonomie technologique entre ses différents systèmes. En respectant une interface standardisée, un système peut être remplacé par un autre de manière quasi transparente.

L’approche d’interopérabilité entre systèmes : 

* permet une communication et une collaboration simplifiées et facilitées entre différentes applications, plateformes ou systèmes d’information,  
* stimule l'innovation et la créativité de nouvelles fonctionnalités pour les usagers, tout en facilitant l'évolutivité des systèmes conformes aux interfaces associées, 
* favorise la complémentarité et une mise en concurrence équitable des systèmes au sein de l'écosystème,
* prévient la prolifération de connecteurs spécifiques et contribue à maîtriser les coûts pour l’ensemble des acteurs.

L’interopérabilité est ainsi primordiale afin de tirer parti des avantages offerts par diverses technologies pour créer des environnements informatiques plus flexibles, fluides et efficaces pour l’utilisateur final. 

## 2.2. Niveaux d'interopérabilité

Comme cela a été identifié par l’[EIF (European Interoperability Framework)](https://joinup.ec.europa.eu/collection/interoperable-europe/interoperable-europe) et rappelé dans le RGI (Référentiel Général d’Interopérabilité), plusieurs niveaux d’interopérabilité sont à prendre en considération dès que l’on souhaite faire communiquer deux systèmes entre eux.

![Figure 1 : Définition et niveaux d'interopérabilité](Figures/Figure2.2.png "Figure 1 : Définition et niveaux d'interopérabilité")
*Figure 1 : Définition et niveaux d'interopérabilité*

1. Le niveau politique apporte une stratégie convergente et des visions partagées entre les parties prenantes, favorisant ainsi la mise en œuvre des solutions d’échange.
2. Le niveau juridique garantit que les informations échangées seront bien en accord avec le cadre légal et réglementaire et les accords contractuels établis. Il doit ainsi notamment prendre en considération les recommandations de la CNIL et de l‘ANSSI.
3. Le niveau organisationnel définit les moyens et les procédures mis en œuvre pour permettre les échanges et l’interopérabilité entre les systèmes.
4. Le niveau sémantique permet une compréhension partagée des différents éléments de l’échange entre les tiers concernés. Elle pourra ainsi reposer sur un ensemble de nomenclatures partagées entre les acteurs.
5. Le niveau syntaxique précise le format des données échangées.
6. Le niveau technique définit enfin les caractéristiques techniques de l’échange comme les protocoles et moyens de transport.

## 2.3. Nomenclatures et vocabulaires

L’interopérabilité entre services ou systèmes d’information hétérogènes et dans les systèmes d’échange nécessite de poser et maintenir un contenu informationnel compréhensible par les différents partenaires.

Les spécifications sémantiques d’interopérabilité imposent de définir un langage commun permettant aux applications des systèmes d’information participants d’interpréter de façon homogène la nature et les valeurs des données transmises et de les réutiliser sans erreur ou perte d’information.

Dans le champ de l’éducation, différentes nomenclatures permettent de répondre à ce besoin.

Les nomenclatures de la BCN (Base Centrale des Nomenclatures) constituent le langage commun du système d'information du ministère chargé de l’éducation nationale. Elles sont mises à jour régulièrement et historisées au moyen de dates d'ouverture et de fermeture. Elles sont en consultation sur le site de la [Base centrale des nomenclatures](http://infocentre.pleiade.education.fr/bcn/ "Base centrale des nomenclatures").

En dehors de son profil d’application, [ScoLOMFR](https://eduscol.education.fr/1086/le-referentiel-scolomfr "ScoLOMFR") fournit des vocabulaires communs aux acteurs sur les principaux aspects de l’activité et de l’enseignement scolaire. Il est à la disposition de l'ensemble de la communauté scolaire.

## 2.4. Principes et outils pour les échanges de données

Pour que les échanges de données entre acteurs s’opèrent dans de bonnes conditions afin d’assurer une interopérabilité effective, des principes doivent être partagés et respectés.

Un outillage pour la mise en œuvre de ces principes est alors possible, sur la base d’API, dont la gestion massive pour l’intégration des services peut être facilitée via l’utilisation d’un API *manager*.

### 2.4.1. Un ensemble cohérent, du dictionnaire de données aux protocoles recommandés

La proposition représentée ci-dessous s’inspire des travaux de [Brandt Redd](https://brandtredd.org/) qui sont exposés sur [EdMatrix](https://www.edmatrix.org/) sous licence Creative Commons 4.0. La conception des données requiert une organisation en quatre couches. Elles ne sont pas nécessairement présentes dans toutes les normes mais chacune d’entre elles doit être prise en compte pour s’intégrer dans un système fonctionnel.

![Figure 2 : Conception de l'organisation des données](Figures/Figure2.4.1.png "Figure 2 : Conception de l'organisation des données")
*Figure 2 : Conception de l'organisation des données*

**Dictionnaire de données** : Il s'agit d'une liste de données, chacune ayant un titre, une définition et parfois un format. Par exemple, Titre : "Date de naissance" ; Définition : "Jour de naissance d'un individu" ; Format : "année-mois-jour".

**Modèle logique de données** : Un modèle logique de données définit la structure des données, en décrivant les relations entre les données, regroupées en ensembles cohérents (objets ou entités). Par exemple, une entité "Élève" peut inclure les propriétés "nom", "date de naissance", "sexe", "adresse", etc. Le type d'entité "Étudiant" aurait une relation **de plusieurs à plusieurs** avec l'entité "Classe".

**Format de données** : Il s'agit du codage des informations représentatives d’un objet sous la forme d'une suite d'informations plus petites (« atomiques ») pour permettre la transmission d’informations structurées ou leur stockage. Les deux formats XML et JSON, par exemple, sont en concurrence pour les formats de codage textuel mais des formats personnalisés peuvent exister et s’appliquer à un même modèle de données. 

**Protocole :** Il dicte les règles de communication entre deux systèmes afin d’assurer que les données échangées soient envoyées et reçues sans perte d’octets, sans erreur, sans duplication et dans l’ordre dans lequel elles ont été envoyées. 

### 2.4.2. Mise en œuvre des échanges par API

Si un protocole est un ensemble de règles qui régissent la manière dont les données sont transmises sur un réseau, une API est une interface qui permet à deux applications différentes de communiquer entre elles. La mise en œuvre des échanges de données par API permet d'assurer la fluidité des échanges entre applications, services et ressources et d’intégrer efficacement des services numériques à partir d’une ou plusieurs plateformes dans un parcours de navigation optimisé pour l’élève, l’enseignant ou tout autre acteur de la communauté éducative. La collaboration et le partage des ressources s’en trouvera facilité.

Les API doivent être accompagnées d’une documentation complète et claire par le fournisseur ainsi que d’un support technique efficace afin de faciliter leur adoption et leur utilisation par les développeurs.

Elles sont conçues pour être flexibles et évolutives afin de répondre aux besoins actuels et être en capacité de s’adapter aux besoins futurs.