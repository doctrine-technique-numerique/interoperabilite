# 5. Principes d’urbanisation communs 

Pour une approche urbanisée des échanges, nécessaire pour décloisonner le secteur des services numériques pour l’éducation, des vocabulaires communs sont nécessaires et des interactions via des API permettront d’assurer la fluidité des échanges de données et l’intégration entre services.

## 5.1. Nomenclatures des objets d’éducation 

Des nomenclatures nationales permettent de proposer un vocabulaire de base, en français, permettant aux acteurs de partager la même compréhension du concept manipulé, indépendamment du système d’où il provient. Ces vocabulaires partagés sont essentiels pour améliorer l'interopérabilité sémantique des services interconnectés.

![Figure 6 : Positionnement des nomenclatures](Figures/Figure5.1.png "Figure 6 : Positionnement des nomenclatures")  
*Figure 6 : Positionnement des nomenclatures*

!!!regle "Exigence"
    Les services numériques pour l’éducation dans les 1<sup>er</sup> et 2<sup>d</sup> degrés décrivent les personnes et structures pédagogiques de manière identique sur la base des nomenclatures de la BCN (Base Centrale des Nomenclatures).


## 5.2.	Échanges de données par API 

!!! regle "Exigence"
    Les services numériques pour l’éducation sont API-sés en exposant des APIs sécurisées pour d’autres services ou applications consommatrices. Ils permettent également de consommer des APIs fournies par des services ou applications tierces. Les services permettent également l’accès à d’autres services en utilisant des APIs. 

!!! regle "Exigence"
    OMOGEN est le point d’entrée et de sortie unique pour les échanges de données publiques *via* les API avec les SI en académie, avec les partenaires externes et au national.

## 5.3.	Échanges de fichiers pour les systèmes de stockage

!!!regle "Exigence"
    Les systèmes de stockage de fichiers permettent le partage et l'échange de fichiers au format Objet en utilisant le protocole REST ou S3

## 5.4. Normes et standards

Le tableau ci-dessous présente les normes et standards utilisables ainsi que leur statut pour l’échange et la circulation des données.

| Champs | Organisme | Standards | Observation | Statut |
|--------|-----------|-----------|-------------|--------|
| Protocole	| IETF	| HTTPS |	HyperText Transfer Protocol Secure : combinaison du HTTP avec une couche de chiffrement TLS.	| Cible |
| Protocole	| |	REST | Representational state transfer. Style d' architecture logicielle définissant un ensemble de contraintes à utiliser pour créer des services web | Cible |
| Format |	IETF |	JSON |	JavaScript Object Notation. Format de données textuel dérivé de la notation des objets du langage JavaScript.	| Cible |
| Format |	W3C |	XML |	Extensible Markup Language. L'objectif initial de XML est de faciliter l'échange automatisé de contenus complexes entre systèmes d'informations hétérogènes	| En usage |