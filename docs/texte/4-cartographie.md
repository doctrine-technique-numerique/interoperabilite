# 4. Cartographie des données et besoins de standardisation

Pour que les échanges de données entre acteurs permettent d’assurer une interopérabilité effective, des principes doivent être partagés et respectés. Sur la base de la typologie des données présentée au chapitre 2 du présent document, la matrice ci-dessous fournit une grille de positionnement des besoins de standardisation des données.

Elle fait apparaître la nécessité de dictionnaires de données partagés, de règles de structuration des données et de leur échange selon des principes communs, afin de pouvoir les échanger de façon optimisée et dans l’intérêt final de l’usager.

Le chapitre 6 décrit les standards retenus dans la doctrine technique du numérique pour l’éducation, puis en fournit une synthèse selon cette même matrice. 

![Figure 5 : Grille de positionnement des besoins de standardisation des données](Figures/Figure4.0.png "Figure 5 : Grille de positionnement des besoins de standardisation des données")  
*Figure 5 : Grille de positionnement des besoins de standardisation des données*