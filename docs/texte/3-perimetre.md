# 3. Périmètre et principes du référentiel d’interopérabilité des services numériques pour l’éducation

## 3.1.	Périmètre de l’interopérabilité

Le référentiel d’interopérabilité des services numériques pour l’éducation traite des questions d’interopérabilité pour l’ensemble des services numériques pour l’éducation fournis par les acteurs publics ou privés. 

## 3.2.	Typologie des données d’éducation

Afin de pouvoir décrire des règles communes pour l’interopérabilité des services numériques, le schéma ci-dessous propose une grille de description standard des données s’appuyant sur la caractérisation des données d’éducation présentée au chapitre 5 de la doctrine technique

Ces propositions et représentations s’inspire des travaux de [Brandt Redd](https://brandtredd.org/) qui sont exposés sur [EdMatrix](https://www.edmatrix.org/) sous licence Creative Commons 4.0.

![Figure 3 : Grille de description standard des données](Figures/Figure3.2.png "Figure 3 : Grille de description standard des données")  
*Figure 3 : Grille de description standard des données*

Les données standard d’éducation peuvent être réparties en 2 grandes catégories et 8 types : 

* Données liées aux parties prenantes (« données des acteurs »)
   * Données décrivant l’organisation scolaire
   * Données liées à l’identité 
   * Données décrivant les interactions entre les acteurs (données évènementielles), dont font partie les « traces d’apprentissage »
   * Données descriptives des attestations de réussite

* Données liées aux contenus (« données des ressources ou services »)
   * Métadonnées pour référencer et partager de façon certifiée les acquis des personnes 
   * Métadonnées pour décrire, référencer et partager les définitions de compétences
   * Métadonnées de définition des contenus
   * Métadonnées pour le conditionnement et l’intégration des contenus.

## 3.3.	Bénéfice de l’interopérabilité pour les usagers

Le référentiel vise à contribuer à la mise en place d’une **offre de services finale cohérente, centrée sur les besoins de l’usager**. 

Les travaux menés avec les utilisateurs des services numériques pour l’éducation dans le cadre des travaux 2023-2024 du programme Doctrine technique du numérique ont permis de dégager quatre volets d’amélioration liés à l’interopérabilité entre services. 

![Figure 4 : Quatre volets d'amélioration](Figures/Figure3.3.png "Figure 4 : Quatre volets d'amélioration")  
*Figure 4 : Quatre volets d'amélioration*

### 3.3.1. Authentification unique

Une authentification unique des utilisateurs est nécessaire pour : 

* assurer une simplicité d’accès aux services numériques et favoriser la fluidité du parcours usagers entre plusieurs services (principe du « dites-le-nous une fois ») ;
* contribuer à la sécurité du service et du parcours usager ;
* contribuer à la défendabilité du service.

Les solutions (standards et services socles du périmètre de l’éducation décrit dans le chapitre 4 de la doctrine technique) utilisées pour l’authentification et la propagation des données d’identité ont un impact sur les autres volets.

### 3.3.2. Portabilité des données des acteurs

La portabilité des données doit être définie dès l’initialisation du contrat, elle est nécessaire pour : 

* assurer la continuité du parcours usager entre années et cycles scolaires ;
* assurer la continuité du parcours usager entre établissements ou académies ;
* assurer la continuité du parcours usager lors de changement de fournisseurs de services ;
* garantir l'authenticité, l'intégrité, la disponibilité de la donnée, et assurer la non répudiation ;
* garantir l'export de la donnée dans un format lisible.

### 3.3.3. Circulation des données entre services numériques

La circulation des données, de façon maîtrisée, entre services numériques pour l’éducation est nécessaire pour : 

* assurer le service public du numérique éducatif pour les activités pédagogiques et les missions éducatives et les démarches administratives (principe du « dites-le-nous une fois ») ;
* créer de nouvelles fonctionnalités au bénéfice des usagers et de l’écosystème ;
* renforcer les possibilités créées par le numérique de personnalisation des parcours, de suivi des apprentissages et de remédiation par l’enrichissement des outils ;
* assurer le suivi de l’élève, quelle que soit l’activité qu’il a réalisée et quel que soit l’éditeur qui met à disposition le service numérique correspondant ;
* garantir la conservation, maintenir l’intégrité, l’authenticité et la non répudiation des données produites par l’usager pour son propre usage.

### 3.3.4. Intégration de contenus éditoriaux

L’intégration de contenus éditoriaux entre services et ressources numériques est nécessaire pour : 

* produire des parcours usagers fluides,
* favoriser la complémentarité entre services.

## 3.4. Critères de sélection des standards

À l’instar des critères d’adoption retenus pour le [RGI](https://www.numerique.gouv.fr/uploads/Referentiel_General_Interoperabilite_V2.pdf "référentiel général d'interopérabilité"), les standards sélectionnés dans le présent document répondent aux critères suivants : 

* ouvert,
* pertinent,
* mature,
* indépendant,
* facile à déployer, 
* soutenu par l’industrie.

Par ailleurs, à chaque standard est associé un statut pour faciliter la prise en compte par les fournisseurs de services, assurer une période de transition et de mise en conformité progressive. Le statut « émergent » permet également de prendre en compte les innovations technologiques.

|Statut       | Explication du statut|
|-------------|----------------------|
|Cible        | Il s’agit d’un standard qui doit être respecté et appliqué par tous.|
|En usage     |Il s’agit d’un standard actuellement en usage au sein des systèmes d’information d’éducation.|
|Émergent     |Il s’agit d’un standard en émergence ou dont la maturité, la mise en œuvre et le soutien par la recherche et/ou l’industrie ne sont pas totalement acquis.|
|En fin de vie| Il s’agit d’un standard en fin de vie, dont le soutien se termine car d’autres standards de remplacement émergent. Son application est à prendre avec précaution ; il est donc considéré « en sursis ». Si son retrait n’a pas encore été demandé dans tous les systèmes existants, il ne doit cependant pas être utilisé pour les nouveaux projets.|