# 8. Cas d'usage

## 8.1. Accéder à une ressource pédagogique

### Description

Un élève souhaite accéder à une ressource numérique depuis son parcours pédagogique. 

### Exemple de scénario d’accès via le GAR

* L’élève se connecte sur le service numérique de son établissement (ENT/OVS/LMS) via ÉduConnect 
* À partir de son parcours pédagogique, l’élève accède à une ressource numérique (via un lien profond)
* Une session d’apprentissage s’ouvre sur le fournisseur de ressources depuis le navigateur de l’élève

### Hypothèses

* Les responsabilités liées à l’accès et l’utilisation des ressources sont encadrées par le contrat GAR / RGPD
* Le fournisseur de ressources est accessible via une URL du GAR
* L’authentification unique (SSO) réalisée par l’élève via ÉduConnect permet au GAR d’identifier l’élève
* La ressource permet un accès granulaire
* Pas d’utilisation d’outil propriétaire pour la visualisation de contenu

### Normes et standards concernés par ce cas d’usage

| Source | Destination | Protocole de transport | Norme d’échange | Contrat d’interface | Description |
|---|---|---|---|---|---|
| ENT/OVS/LMS | ÉduConnect | HTTPS-REST | OIDC | ÉduConnect | Identité de l’élève fournie par ÉduConnect après authentification |
| GAR | ENT/OVS/LMS | HTTPS | LTI | GAR | L’accès à la ressource est réalisé via une URL GAR. Celui-ci relaie l’accès au fournisseur de ressources après un contrôle d’autorisation |
| ENT/OVS/LMS | Fournisseur de ressources | HTTPS | LTI | À définir | Ouverture d’une session LTI entre le navigateur de l’élève et le serveur du fournisseur de ressources |
|ENT/OVS/LMS | ENT/OVS/LMS | REST | xAPI | À définir | Enregistrer les traces d’apprentissage |


## 8.2.	Faciliter l’intégration des ressources dans un LMS pour une utilisation sans couture dans un parcours pédagogique

### Description

Un enseignant souhaite créer un parcours pédagogique basé sur des ressources numériques fournies par plusieurs éditeurs tiers.

### Exemple de scénario d’accès via le GAR

* L’enseignant se connecte sur le service numérique de son établissement (ENT/OVS/LMS).
* Il construit son parcours pédagogique en utilisant une liste de ressources accessibles, remontée par le GAR dans le médiacentre proposé par le service numérique

### Hypothèses 
* Le GAR est le fournisseur de métadonnées sur les ressources
* Le GAR reconnait l’identité de l’enseignant connecté sur son outil numérique
* La liste de ressources remontée par le GAR est à la fois accessible par l’enseignant et par les élèves auxquels est destiné le parcours pédagogique
* L’ENT est le fournisseur de l’identifiant de l’utilisateur pour le GAR.

### Flux technique et normes

| Source | Destination | Protocole de transport | Norme d’échange | Contrat d’interface | Description |
|---|---|---|---|---|---|
| Guichet Agents | ENT/OVS/LMS | REST  | OpenID Connect | Guichet Agents | Authentification et identification de l’enseignant |
| GAR | ENT/OVS/LMS | REST | ScoLOMFR | GAR | Fourniture de la liste de ressources accessibles à l’enseignant |


## 8.3.	Assurer la portabilité des contenus d’un parcours pédagogique lors d’un changement de LMS

### Description

Un enseignant / un personnel de direction souhaite, dans le cadre d’un changement d’établissement, faire le transfert et la reprise de ses données depuis la solution numérique de l’établissement de départ jusqu’à la solution numérique de l’établissement d’arrivée.

### Exemple de scénario exécuté sur l’ENT de l’établissement de départ

* ​L’enseignant/PERDIR se connecte sur l’ENT de son établissement de départ puis saisit une demande de changement d’établissement et de transfert de son dossier vers un établissement d’arrivée. Il sélectionne ensuite une liste des contenus à transférer.
* L’ENT de départ envoie une demande à l’ENT d’arrivée avec la liste des contenus (cahier de texte, messagerie, parcours Moodle, ressources pédagogiques, etc..) et leurs formats sources.
* L’ENT d’arrivée analyse la demande et répond avec la liste des contenus et formats compatibles.

### Exemple de scénario exécuté sur l’ENT de l’établissement d’arrivée

* L’enseignant/PERDIR se connecte sur l’ENT de son établissement d’arrivée
* L’ENT télécharge et intègre automatiquement les données dont le format est compatible avec la solution ENT de l’établissement d’arrivée.
* L’enseignant/PERDIR réalise le téléchargement manuel des contenus dont le format n’est pas compatible avec l’ENT de l’établissement d’arrivée. L’opération est exécutée via l’IHM de l’ENT de l’établissement d’arrivée

### Hypothèses

* Les ENT de départ et d’arrivée sont capables de reconnaitre l’identité de l’enseignant/PERDIR demandant le transfert.

| Source | Destination | Protocole de transport | Norme d’échange | Contrat d’interface | Description |
|---|---|---|---|---|---|
| Guichet Agents | ENT1 (départ) / ENT2 (arrivée) | REST | OpenId Connect | Guichet Agents : +lien | Fourniture de l’identité de l’enseignant |
| ENT1 (départ) | ENT2 (arrivée) | REST | JSON | Contrat d’interface unique pour les ENT : +lien | Liste des contenus avec leur formats locaux |
| ENT2 (arrivée) | ENT1 (départ) | REST | JSON | Contrat d’interface unique pour les ENT : +lien | Liste des contenus avec les formats compatibles |
| ENT1 (départ) | ENT2 (arrivée) | REST / S3 | Binaire, ScoLOMFR, Schema.org | Contrat d’interface unique pour les ENT : +lien | Téléchargement des contenus binaires, ou formatés comme les liste d’indexes SCOLORMFR/Schema.org |
