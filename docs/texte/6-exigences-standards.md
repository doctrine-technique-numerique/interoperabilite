# 6. Présentation des exigences et standards retenus par domaine fonctionnel

Ce chapitre permet de présenter l’ensemble des exigences pour les services numériques pour l’éducation par domaine fonctionnel et type de données.

## 6.1. Identification et authentification des utilisateurs dans un contexte interopérable

L’authentification des utilisateurs est opérée par un service numérique « d’identification/authentification » qui fonctionne à partir de la réception et de la vérification d’un couple « identifiant / authentifiant ». Il permet également la gestion du cycle de vie des identités et des authentifiants.

L’identification permet de connaître l’identité d’un utilisateur, alors que l’authentification permet de vérifier l’identité.

Il existe plusieurs facteurs d’authentification : utiliser une information que seul le prétendant connaît (mot de passe), possède (carte à puce), est (données biométriques), peut produire (un geste).

Les protocoles d’authentification décrivent les interactions entre un prouveur et un vérifieur et les messages permettant de prouver l’identité d’un utilisateur. On distingue deux familles de protocoles d’authentification : l’authentification simple (un seul facteur d’authentification en jeu) et l’authentification forte (deux facteurs ou plus). Par ailleurs, on parle d’authentification unique lorsqu’un utilisateur n’a besoin de procéder qu’à une seule authentification pour accéder à plusieurs applications informatiques.

!!!regle "Exigence"
    Les services numériques pour l’éducation implémentent les fonctions de propagation d’identité SSO (*single sign-on*). Le système permet l’accès à un service externe puis de revenir sur le premier service, par échange de données d’identité.

### 6.1.1. Fournisseurs d’identité du périmètre des services numériques pour l’éducation (services socles)

La doctrine technique du numérique pour l’éducation décrit, dans son chapitre 4, les services d’identification/authentification ÉduConnect pour les élèves et leurs représentants légaux, et le guichet Agents pour les agents de l’éducation nationale.

Ils permettent à ces utilisateurs de fédérer leur identité entre les différents portails de services raccordés.

### 6.1.2. Interface avec les services socles fournisseurs d’identité

!!! regle "Exigence"
    Les services numériques pour l’éducation utilisent les services socles pour l’authentification/identification des usagers du périmètre éducatif.

#### 6.1.2.1. Contrats d’interface ÉduConnect

Les contrats d’interface ÉduConnect sont disponibles sur demande, lors des travaux d’accrochage.

L’interconnexion des services numériques pour l’éducation avec le guichet ÉduConnect, sous réserve qu’ils soient sous la responsabilité de traitement du ministère de l’Éducation nationale (ministère, académies, chefs d’établissement, IA-DASEN) et déclarés dans les registres de traitement des entités concernées, s’effectue par la mise en place d’une fédération d’identité. 

#### 6.1.2.2. Contrats d’interface du guichet Agents

!!! example ""
    À venir

### 6.1.3. Normes et standards

!!! regle "Exigence"
    Pour la fédération d’identités permettant l’authentification SSO, les services numériques pour l’éducation s’appuient sur des protocoles standards et éprouvés tels que décrits dans le tableau ci-dessous.  
    Les appels aux APIs utilisent une authentification basée sur des protocoles d’autorisation et d’authentification tels que OAuth2 ou OIDC.

Le tableau ci-dessous présente les normes et standards utilisables ainsi que leur statut pour l’identification et l’authentification des utilisateurs.

| Champs	| Organisme	| Standard	| Observation	| Statut |
|-----------|-----------|-----------|---------------|--------|
| Protocole |	OIDF |	Open ID Connect (OIDC) |	Couche d'identification basée sur OAuth 2.0, dispositif d'autorisation.	| Cible |
| Protocole	| Internet engineering task force (IETF) |	OAuth2 |	Protocole d’autorisation. Inclus dans OIDC |	Cible |
| Protocole	| Organization for the Advancement of Structured Information Standards (OASIS)	| SAML 2.0 |	Utilisé par le GAR. Permet l’échange de données d’authentification et d’identification entre domaines de sécurité.	| En usage |
| Protocole	| APEREO	| CAS	| Central Authentification Service (système d’authentification unique). Support OIDC, OAuth2 et SAML	| Fin de vie |

## 6.2.	Organisation pédagogique pour la vie scolaire

Les fonctions correspondant aux processus et activités métiers s’appuyant sur l’organisation pédagogique visent à matérialiser et mettre à jour l’organisation scolaire optimisée dans l’espace et le temps pour assurer les activités pédagogiques et missions éducatives, c’est-à-dire l’emploi du temps. Les données d’organisation scolaire sont nécessaires pour les fonctions de suivi de l’assiduité des élèves (absences, évaluations…) telles que définies au code de l’éducation, mais également pour les espaces numériques de travail, les systèmes d’information d’évaluation ou de certification.

!!! regle "Exigence"
    Les services numériques pour l’éducation outillant le système d’information pédagogique de l’établissement implémentent les fonctions d’échanges de données d’organisation scolaire avec les autres services numériques habilités. 

### 6.2.1. Périmètre des services numériques pour l’éducation

Les fournisseurs de solutions d’emploi du temps et/ou de vie scolaire outillent les processus et activités s’appuyant sur l’organisation pédagogique définie par le chef d’établissement.

La doctrine technique du numérique pour l’éducation décrit, dans son chapitre 4, le service pour la circulation des données d’organisation pédagogique (Scope).

### 6.2.2. Interface entre les fournisseurs de données et fournisseurs de service du périmètre

#### 6.2.2.1. Échanges par API normalisées

!!! regle "Exigence"
    Les différents fournisseurs de données ou de services outillant les établissements et écoles pour leurs processus et activités pédagogiques permettent des communications basées sur des API normaliséeset apportant des mesures de sécurité prouvant l'authentification des sujets, l'intégrité et la confidentialité des échanges. 

#### 6.2.2.2. Contrats d’interface Scope

!!! regle "Exigence"
    Les services numériques pour l’éducation utilisent les services socles pour la circulation des données d’organisation pédagogique du périmètre éducatif.

##### A. Emplois du temps ("Scope entrée")

Le contrat d’interface unique pour la remontée des données d’organisation pédagogique, entre le MENJ pour le service Scope et chaque acteur fournissant un service d’élaboration et de gestion d’emploi du temps, est disponible pour les acteurs habilités dans l’espace Pleiade[^1] [« ESPACE ÉDITEURS INTERFACE ÉDUCATION NATIONALE » / rubrique « emploi du temps](https://www.pleiade.education.fr/sites/002233/) ».

[^1]: Les éditeurs peuvent demander à s’y inscrire en écrivant à [communication-editeurs(arobase)education.gouv.fr](mailto:communication-editeurs@education.gouv.fr) 

##### B. Contrat d’interface Scope – Services habilités (« Scope sortie »)

Les contrats d’interface entre le service Scope du MENJ et chaque acteur habilité à récupérer des données issues des emplois du temps opérationnels seront disponibles à titre expérimental sous la seule forme d’API dans un premier temps. L'API Scope proposera plusieurs services (souscription sur demande à [osmose.coord-gen(arobase)ac-dijon.fr](osmose.coord-gen@ac-dijon.fr)), répondant chacun à des cas d’usage préalablement concertés avec les représentants des personnels de direction d’EPLE.

### 6.2.3. Normes et standards

!!! regle "Exigence"
    Pour la circulation des données d’organisation pédagogique, les services numériques pour l’éducation s’appuient sur des standards et éprouvés tels que EML (pour les messages de type courrier électronique), iCal (pour les invitations Agenda), vCard (pour les informations de contacts des utilisateurs) ou encore OneRoster (pour les listes de cours et les données associées).

Le tableau ci-dessous présente les normes et standards utilisables, ainsi que leur statut, sur le périmètre de l’organisation pédagogique pour la vie scolaire.

| Champs	| Organisme	| Standards	| Observation	| Statut |
|---|---|---|---|---|
| Protocole / format |	1EdTech 	| OneRoster	| Profil français à venir, dans le cadre des travaux avec 1Edtech. Éléments d’échange de toutes les données de vie scolaire	| Cible |
| Format	| IETF	| EML	| Format de fichier pour les messages électroniques	| Cible |
| Format	| Internet Mail Consortium	| vCard	| Format de carnet d’adresses	| Cible |
| Format |	IETF	| iCal	| Internet Calendaring and Scheduling Core Object Specification pour l’échange et le déploiement des événements de calendrier et de planification.	| Cible |

## 6.3.	Ressources et services numériques pour l’enseignement et les pratiques pédagogiques

Les pratiques pédagogiques des enseignants s’appuient sur les programmations pédagogiques annuelles et périodiques (progressions) outillées par des services numériques de type « cahier de textes » pour sérier et organiser les activités associées ainsi que par des ressources numériques éducatives fournissant du contenu et des outils au bénéfice de l’enseignement et de l’apprentissage.

!!! regle "Exigence"
    Les services numériques pour l’éducation fournissant des fonctions de ressources et services numériques pour l’enseignement et les pratiques pédagogiques organisent leurs contenus selon une description sémantique normalisée et permettent l’intégration de granules pour réutiliser facilement des contenus ou des modules d’évaluation, pour rendre compte des expériences d’apprentissage ou encore pour assurer le suivi en continu de l’activité des élèves.

### 6.3.1. Fournisseurs de ressources et services numériques pour l’enseignement et les pratiques pédagogiques 

Les fonctions d’enseignement et pratiques pédagogiques sont outillées par de nombreux ressources ou services numériques, dont le choix est opéré par les enseignants au titre de leur liberté pédagogique, exercée dans le cadre de leur établissement/école eux-mêmes bénéficiaires des projets territoriaux de ressources et services numériques.

En raison de la diversité des fournisseurs de services concernés, il est indispensable de partager des principes permettant de trouver, accéder, éditer et intégrer les ressources pédagogiques de manière transparente entre les services ou portails de service, tout en garantissant la protection des données à caractère personnel des élèves et des personnels d’éducation.

La doctrine technique du numérique pour l’éducation décrit, dans son chapitre 4, le service GAR pour la gestion des accès aux ressources, qui permet d’assurer une autorité d’autorisation, en mettant en relation les données des utilisateurs avec les données des ressources.

### 6.3.2. Exigences pour les fournisseurs de données et fournisseurs de service du périmètre

#### 6.3.2.1. Intégration de contenus pour les parcours pédagogiques

!!! regle "Exigence"
    Les services numériques pour l’éducation doivent permettre aux utilisateurs :

    * de construire un parcours pédagogique avec des ressources pédagogiques ;
    * d’utiliser des granules pédagogiques dans un parcours pédagogique sans sortir de celui-ci ;
    * de suivre l’activité de l’élève tout au long d’un parcours.

#### 6.3.2.2. Portabilité et réversibilité

!!! regle "Exigence"
    Les services numériques pour l’éducation doivent permettre aux utilisateurs :
    
    * de créer des parcours pérennes et réutilisables, donc indépendants des différents acteurs ;
    * d’exporter et d’importer des granules ou des parcours pédagogiques.

#### 6.3.2.3. Échanges de données à caractère personnel – contrat d’interface GAR

!!! regle "Exigence"
    Les services numériques pour l’éducation utilisent les services socles pour la gestion des accès aux ressources (GAR).

Les contrats d’interface permettant le raccordement au GAR sont fournis par le RTFS (référentiel technique, fonctionnel et de sécurité) du GAR. Celui-ci s’adresse aux partenaires, fournisseurs de ressources d’une part et projets ENT d’autre part. Il est organisé en trois opuscules et un paquetage :

* opus « Présentation générale » ;
* opus « Référentiel juridique et administratif » ;
* opus « Référentiel technique » ;
* paquetage de documents d’accompagnement et exemples.

Le RTFS comporte une version pour les fournisseurs d’identité, notamment les projets ENT et leurs exploitants, et une version pour les fournisseurs de ressources.

Le RTFS intègre le contrat d’interface des exploitants ENT ou des fournisseurs de ressources.

Il est disponible sur demande à l’adresse [dne-gar(arobase)education.gouv.fr](mailto:dne-gar@education.gouv.fr) ou sur le [site du GAR](https://gar.education.fr/documentation-projet/), dès la demande d’accrochage. 

### 6.3.3. Normes et standards

!!! regle "Exigence"
    Pour répondre aux exigences d’intégration de contenus, de portabilité et de réversibilité, les services numériques pour l’éducation s’appuient sur des protocoles standards et éprouvés tels que xAPI ou LTI.

#### Normes et standards utilisables pour l’enseignement avec leur statut

##### Suivre l’activité de l’élève tout au long d’un parcours (traces d’apprentissage)

| Champs	| Organisme	| Standard	| Observations	| Statut |
|---|---|---|---|---|
| Données évènementielles / Protocole |	ADIL/IEEE	| xAPI	| Tin Can/xAPI est un standard paramétrable qui peut être utilisé dans tous les contextes (services numériques, plateformes pédagogiques, ressources numériques, évaluations formatives et sommatives, interactions pair à pair) pour collecter des données évènementielles permettant de rendre compte du parcours d’un élève en le suivant pas à pas, et des usages d’une plateforme, d’un service ou d’une ressource. |	Cible |
| Données évènementielles / Protocole |	1EdTech	| Caliper Analytics	| Le standard Caliper offre un cadre normalisé et largement adopté pour la collecte et l'analyse des données événementielles liées à l'apprentissage, favorisant ainsi une meilleure compréhension de l'activité des élèves tout au long de leur parcours éducatif. |	Cible |

##### Exporter et importer des granules ou des parcours pédagogiques

| Champs	| Organisme	| Standard	| Observations	| Statut |
|---|---|---|---|---|
| Métadonnées de définitions des contenus / Modèle logique de données et sérialisation	| 1EdTech	| Common Cartridge 	| Une norme de packaging des contenus pour échanger des contenus et des parcours pédagogiques.	| Cible |
| Données pour le conditionnement et l’intégration des contenus / Modèle logique de données et sérialisation	| ADL/IEEE	| Scorm	| Format pour développer et publier des ressources d'apprentissage numériques réutilisables dans des systèmes de gestion de l'apprentissage et d'autres environnements d'apprentissage.	| En fin de vie |

##### Utiliser des granules pédagogiques dans un parcours pédagogique

| Champs	| Organisme	| Standard	| Observations	| Statut |
|---|---|---|---|---|
| Données pour le conditionnement et l’intégration des contenus / Modèle logique de données et sérialisation	| Groupe H5P	| H5P	| H5P est un plugin pour les systèmes de publication existants qui permet aux utilisateurs de créer du contenu interactif comme des vidéos interactives, des présentations, des jeux, des quiz et bien d’autres.	| Cible |
| Données pour le conditionnement et l’intégration des contenus	| 1EdTech	| QTI	| Format d’échange de questions et de structures de test, d'informations sur la notation	| Cible |
| Données pour le conditionnement et l’intégration des contenus / Protocole	| 1EdTech	| LTI	| Protocole permettant à un système de gestion de l'apprentissage d'intégrer des outils d'apprentissage personnalisés ou des expériences proposées par un autre système.	| Cible |

##### Décrire des ressources pédagogiques 

| Champs | Organisme	| Standard	| Observations	| Statut |
|---|---|---|---|---|
Métadonnées de définitions des contenus / Dictionnaires de données, modèle logique de données et sérialisation	| IEEE / AFNOR	| ScoLOMFR	| Description des ressources. | Cible |

## 6.4.	Examens, certifications et concours

Le tableau ci-dessous présente les normes et standards utilisables ainsi que leur statut pour les examens, certifications et concours.

| Champs | Organisme | Standard | Observations | Statut |
|---|---|---|---|---|
| Modèle de données	| Commission Européenne	| European Learning Model (ELM)	| Modèle de données multilingue pour l'interopérabilité des possibilités d'apprentissage, des qualifications, de l'accréditation et des titres de compétences en Europe. | Cible |
| Modèle de données | 1EdTech | Open Badge | Méthode pour regrouper des informations sur une reconnaissance ou une réalisation unique, telle qu'une microcertification, une aptitude, une compétence ou un diplôme.	| Cible |

## 6.5.	Pilotage des missions éducatives

### 6.5.1. Fournisseurs de données et de services pour l’évaluation des services numériques pour l’éducation

Le pilotage des missions éducatives relève de différents niveaux de décision, de l’établissement ou de l’école, jusqu’aux services du ministère chargé de l’éducation nationale, en passant par les académies et collectivités territoriales.

Des dispositifs d’évaluation permettent d’outiller les besoins de pilotage de chacun de ces niveaux de décision, afin d’apporter une aide au pilotage de la politique publique d’éducation et des projets de services numériques pour l’éducation en particulier.

### 6.5.2. Exigences pour les fournisseurs de ressources et services numériques pour l’éducation

#### 6.5.2.1. Utilisation d’indicateurs de fréquentation normés

Le nombre de visites est l’indicateur standard des outils de mesure d’audience. Il représente la consultation de pages d'un site. L’utilisation de services numériques pour l’éducation nécessitant la plupart du temps une authentification, chaque visite est effectuée par un utilisateur.

!!!regle "Exigence"
    Pour répondre aux besoins de pilotage des projets et politiques numériques, les services numériques pour l’éducation sont capables de fournir *a minima* deux indicateurs principaux : la visite et l’utilisateur.

Si un utilisateur reste inactif plus de trente minutes avant de reprendre son activité, il génère deux visites. Si sa période d’inactivité est inférieure à trente minutes, il ne génère qu’une visite.

Si deux utilisateurs se connectent depuis le même ordinateur (ou autre appareil) ; ils génèrent chacun une visite et sont considérés comme différents.

![](Figures/Figure6.5.2.1.png)

#### 6.5.2.2. Intégration dans le DNMA – dispositif de mesure d’audience 

Le DNMA est défini comme service socle au chapitre 4 de la doctrine technique du numérique pour l’éducation.

Les services de type ENT qui s’inscrivent dans le DNMA bénéficient du cadre normalisé retenu pour ce dispositif. La définition des indicateurs produits par ce dispositif est celle retenue par [l'ACPM](https://www.acpm.fr/) (alliance pour les chiffres de la presse et des médias) et l'outil de recueil des indicateurs est certifié par cette association professionnelle française.

Le DNMA repose sur une solution de « marquage » externe conforme à un cahier des charges partagé qui définit le contenu et la structure des marqueurs. Le cahier des charges est composé :

* du référentiel fonctionnel et technique d’intégration (RFTI) :
     * description des indicateurs utilisés,
     * liste de référence des indicateurs « service » et « outil »,
     * informations techniques et fonctionnelles nécessaires à la mise en œuvre du marqueur DNMA,
     * description des exigences pour être conforme au DNMA,
     * description des options d’implémentations complémentaires ;

* du plan de marquage :
     * il s’agit de la table de correspondance entre les pages et fonctionnalités de la plateforme ENT et le référentiel des services du dispositif qui permet d’interpréter les statistiques du dispositif ;
     * le plan de marquage est propre à chaque plateforme ENT ;
     * le respect du plan de marquage est une exigence de conformité au DNMA.

### 6.5.3. Normes et standards pour la mesure de la fréquentation

!!! regle "Exigence"
    Pour répondre aux besoins de pilotage et fournir des indicateurs de fréquentation normés, les ressources et services numériques pour l’éducation s’appuient sur des indicateurs de l'ACPM inscrit dans le DNMA.
