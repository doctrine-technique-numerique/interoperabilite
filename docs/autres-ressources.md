# Ressources

## Documents en rapport avec la doctrine technique du numérique pour l'éducation

* [Version en PDF, disponible sur Éduscol](https://eduscol.education.fr/document/49067/download?attachment)
* Grilles de synthèse de l'appel à commentaires 2023
  * [Partie 1](https://eduscol.education.fr/document/49580/download)
  * [Partie 2](https://eduscol.education.fr/document/49583/download)


